#!/usr/bin/env python
#--*-- coding: utf-8 --*--


import os
import Pyro4
import re
import sqlite3
import thread
import time
import datetime
from flask import Flask
import httpagentparser
import tempfile
import logging
from logging.handlers import RotatingFileHandler

from subprocess import Popen, PIPE
class Cmd(object):
   def __init__(self, cmd):
       self.cmd = cmd
   def __call__(self, *args):
       command = '%s %s' %(self.cmd, ' '.join(args))
       result = Popen(command, stdout=PIPE, stderr=PIPE, shell=True)
       return result.communicate()
class Sh(object):
    """Ao ser instanciada, executa um metodo dinamico
    cujo nome serah executado como um comando. Os argumentos
    passados para o metodo serao passados ao comando.

    Retorna uma tupla contendo resultado do comando e saida de erro

    Exemplo:
    shell = Sh()
    shell.ls("/")

    Ver http://www.python.org.br/wiki/PythonNoLugarDeShellScript
    """
    def __getattr__(self, attribute):
        return Cmd(attribute)


app = Flask(__name__)
shell = Sh()


class GCP(object):

    def __init__(self):
        # objeto que executa o iptables no shell
        self.fw_bin = getattr(shell, app.config['FW_PATH'])
        # para thread
        self.threaddaemon_clean_users = None


    def obter_bd(self):
        """
        Ver gcp_web.__init__.py
        """
        # Sempre criar uma conexao com o banco pois a lib sqlite
        # reclama se a conexao for usada em threads diferentes. O Pyro abre uma thread por conexao
        conexao_bd = sqlite3.connect(os.path.join(app.root_path, app.config['GCP_DATABASE']))
        conexao_bd.row_factory = sqlite3.Row
        return conexao_bd


    # def fechar_bd(self):
    #     """
    #     Ver gcp_web.__init__.py
    #     """
    #     conexao_bd = self._CONEXAO_BANCO
    #     if conexao_bd is not None:
    #         conexao_bd.close()


    def buscar_bd(self, query, args=(), apenasUmResultado=False):
        """
        Ver gcp_web.__init__.py
        """
        cursor = self.obter_bd().execute(query, args)
        r = cursor.fetchall()
        cursor.close()
        return (r[0] if r else None) if apenasUmResultado else r


    def validate_mac(self, mac=None):
        if not mac:
            return False
        m = '[0-9a-f]{2}'
        if re.search('{m}:{m}:{m}:{m}:{m}:{m}'.format(m=m),mac, re.I):
            return True
        return False


    def converter_data_hora_utc(self, data_hora=None):
        segundosDiferencaTimezone = time.timezone
        dataHora = datetime.datetime.strptime(data_hora, '%Y-%m-%d %H:%M:%S')
        return dataHora - datetime.timedelta(seconds=segundosDiferencaTimezone)


    def formatar_data_hora(self, data_hora=None):
        d = data_hora
        return "%s/%s/%s %s:%s:%s" % (d.day, d.month, d.year, d.hour, d.minute, d.second)


    # VER: http://www.ibm.com/developerworks/aix/library/au-pythocli/
    def get_mac(self, ip=None, metodo='arp'):
        if not ip:
            return False
        if metodo == 'arp':
            retorno = shell.arp("-a {ip}".format(ip=ip))
            m = '[0-9a-f]{2}'
            mac = re.findall('.*({m}:{m}:{m}:{m}:{m}:{m}).*'.format(m=m), retorno[0], flags=re.I|re.M)
            if mac:
                return mac[0].upper()
            return False


    def fw_read_config(self, key):
        """Lê chave em app.config.
        Retorna uma lista"""
        import re
        ret = []
        if not key in app.config:
            app.logger.error("fw_read_config: chave {} nao existe nas configuracoes".format(key))
            return False
        for r in app.config[key].split("\n"):
            r = r.strip()
            if re.match('^[\t ]*$', r) or re.match('^[\t ]*#', r):
                pass
            else:
                ret.append(r)
        return ret


    def fw_execute_cmd(self, commands):
        """Executa um comando no firewall.

        'commands' pode ser uma string, lista (vide metodo fw_read_config) ou tupla.

        Retorna:
        ret = {
            'result': False,
            'total_errors': 0,
            'messages': {
                'success': [],
                'error': [],
            },
        }"""
        values = []
        ret = {
            'result': True,
            'total_errors': 0,
            'messages': {
                'success': [],
                'error': [],
            },
        }
        if isinstance(commands, str):
            values.append(commands)
        elif isinstance(commands, list) or isinstance(commands, tuple):
            values = commands
        else:
            app.logger.error("fw_execute_cmd: parametro eh do tipo %s que nao eh suportado" % type(commands))
            ret['result'] = False
            return ret

        for command in values:
            r = self.fw_bin(command)
            ret['messages']['success'].append(r[0] or None)
            ret['messages']['error'].append(r[1] or None)
            if r[1] != "":
                app.logger.error("fw_execute_cmd: erro ao executar comando '{}'. Detalhes: {}".format(command, r[1].strip()))
                ret['total_errors'] += 1
        if ret['total_errors'] > 0:
            ret['result'] = False
        return ret


    def fw_format_rule(self, rules, keys=[]):
        """Substitui em 'rules' alguns termos definidos na configuracao.

        'rules' pode ser string, tupla ou lista.

        'keys' é uma tupla contendo dicionarios de chaves e valores adicionais
        que serao substituidos em rules.

        Retorna um valor do mesmo tipo de 'rules'"""
        values = []
        r = []
        if isinstance(rules, str):
            values.append(rules)
        elif isinstance(rules, list) or isinstance(rules, tuple):
            values = rules
        else:
            app.logger.error("fw_format_rule: parametro rules eh do"
                             "tipo %s que nao eh suportado" % type(rules))
            return False

        all_keys = {
            'GCP_PORT': app.config['GCP_PORT'],
            'GCP_LOCAL_IP': app.config['GCP_LOCAL_IP'],
            'FW_CHAIN': app.config['FW_CHAIN'],
            'FW_INTERFACE': app.config['FW_INTERFACE'],
        }
        if keys:
            for item in keys:
                for key, value in item.iteritems():
                    all_keys[key] = value

        for rule in values:
            try:
                r.append(rule % all_keys)
            except KeyError, e:
                app.logger.error("fw_format_rules: Erro. Chave %s faltando" % e)
                # ha uma chave na string que nao pode ser substituida
                return False

        if isinstance(rules, str):
            return str(r[0])
        elif isinstance(rules, tuple):
            return tuple(r)
        else:
            return r


    def fw_delete_rules(self):
        """Deleta as regras criadas pelo metodo fw_create_rules"""
        cmd = self.fw_format_rule(self.fw_read_config('FW_DELETE_RULES'))
        r = self.fw_execute_cmd(cmd)
        # http://serverfault.com/questions/375981/delete-a-iptables-chain-with-its-all-rules
        # uma das regras retorna "Too many links." ver link acima
        if r['total_errors'] == 1:
            r['result'] = True
        return r


    def fw_create_rules(self):
        """Cria as regras no firewall e libera os usuarios com sessão ativa"""
        # deleta as regras
        self.fw_delete_rules()
        # cria as regras iniciais
        create_rules = self.fw_execute_cmd(self.fw_format_rule(self.fw_read_config('FW_INIT_RULES')))
        # reabilita as conexoes ativas
        sessions = self.buscar_bd("""SELECT hosts.mac
        FROM sessions, hosts
        WHERE sessions.host_id = hosts.id
        AND hosts.mac NOT IN (SELECT mac FROM blocked_hosts)
        AND ifnull(sessions.date_time_end, '') = ''
        GROUP by hosts.mac""")
        for session in sessions:
            self.fw_insert_released_host(session['mac'])

        return create_rules


    def fw_list_rules(self):
        """Executa o comando para listar as regras ativas no firewall e
        retorna o resultado no mesmo formato do metodo fw_execute_cmd"""
        return self.fw_execute_cmd(self.fw_format_rule(self.fw_read_config('FW_LIST_RULES')))


    def fw_insert_released_host(self, mac=None):
        """Insere um host liberado no firewall"""
        if not self.validate_mac(mac):
            return False
        comando = self.fw_format_rule(self.fw_read_config('FW_RELEASE_HOST'), keys=[{'MAC': mac}])
        retorno = self.fw_execute_cmd(comando)
        return retorno['result']


    def fw_exclude_released_host(self, mac=None):
        """Exclui um host liberado no firewall"""
        if not self.validate_mac(mac):
            return False
        comando = self.fw_format_rule(self.fw_read_config('FW_EXCLUDE_HOST'), keys=[{'MAC': mac}])
        retorno = self.fw_execute_cmd(comando)
        return retorno['result']


    #DEPRECATED
    # def listar_conexoes_ativas(self):
    #     consultaAtivos = self.buscar_bd("""SELECT ativo.id AS acesso_numero, ativo.data_hora, host.mac, host.descricao_equipamento, host.tipo_equipamento, acesso.endereco_ip, acesso.user_agent
    #     FROM "conexoes_ativas" AS ativo, hosts_cadastrados AS host, acessos AS acesso
    #     WHERE ativo.host_cadastrado = host.id
    #     AND acesso.id = ativo.acesso_numero""")
    #     #FIXME o flask tem um identificador de user agent. Como usar com valor personalizado?
    #     # http://werkzeug.pocoo.org/docs/search/?q=user_agent&check_keywords=yes&area=default
    #     # http://werkzeug.pocoo.org/docs/wrappers/#werkzeug.wrappers.BaseRequest.headers
    #     r = []
    #     r.append("Acesso \t| ativado em \t| MAC \t| descricao equipamento \t| tipo equipamento \t| IP \t| S.O. \t| Browser\t")
    #     for registro in consultaAtivos:
    #         dadosUserAgent = httpagentparser.simple_detect(registro['user_agent'])
    #         data_hora = self.formatar_data_hora(self.converter_data_hora_utc(registro['data_hora']))
    #         #TODO ver modificadores de string para o print
    #         string = "%s \t| %s \t| %s \t| %s \t| %s \t| %s \t| %s\t| %s " % (registro['acesso_numero'], data_hora, registro['mac'],
    #                 registro['descricao_equipamento'], registro['tipo_equipamento'], registro['endereco_ip'], dadosUserAgent[0], dadosUserAgent[1])
    #         r.append(string)
    #     return r


    def clean_expired_session(self, seconds=0):
        """Exclui sessões ativas ha mais tempo do que seconds.
        Se seconds não for informado, lê um dos seguintes valores, na ordem:
        -- 1 - tempo da sessao ou
        -- 2 - tempo do host ou
        -- 3 - tipo de acesso ou
        -- 4 - tempo do acesso padrao
        """
        seconds = int(seconds)
        if not seconds:
            default_time = self.buscar_bd("SELECT personal_time FROM access_types WHERE is_default = 1",
                                          apenasUmResultado=True)
            # nao funcionou: if not 'personal_time' in default_time:
            try:
                seconds = default_time['personal_time']
            except:
                app.logger.warning('clean_expired_session: nao ha chave de tempo padrao definido nos tipos de acesso')
                return False

        if not seconds:
            # 0, null Para casos onde não ha tempo de sessão
            app.logger.debug('clean_expired_session: tempo padrao nao foi definido nos tipos de acesso')
            return True
        conexao = self.obter_bd()
        cursor = conexao.cursor()
        busca_acessos = cursor.execute("""SELECT sessions.id, hosts.mac
            FROM sessions, hosts, access_types
            WHERE sessions.host_id = hosts.id
            AND sessions.access_type_id = access_types.id
            AND hosts.always_released != 1
            AND sessions.date_time_end is null
            AND
            -- onde a data e hora do inicio do acesso for menor que hoje subtraido do numero de segundos de:
            -- 1 - tempo da sessao ou
            -- 2 - tempo do host ou
            -- 3 - tipo de acesso ou
            -- 4 - tempo do acesso padrao ou tempo passado como parametro
            sessions.date_time_begin <
                CASE WHEN ifnull(sessions.personal_time, '') != '' THEN -- 1
                        datetime("now", "localtime", "-" || sessions.personal_time || " seconds")
                    WHEN ifnull(hosts.personal_time, '') != '' THEN -- 2
                        datetime("now", "localtime", "-" || hosts.personal_time || " seconds")
                    WHEN ifnull(access_types.personal_time, '') != '' THEN -- 3
                        datetime("now", "localtime", "-" || access_types.personal_time || " seconds")
                    ELSE -- 4
                        --datetime("now", "localtime", "-" || (SELECT personal_time FROM access_types WHERE is_default = 1) || " seconds")
                        datetime("now", "localtime", "-{s} seconds")
                -- para o caso, inesperado, do campo conter algo que nao um numero (tratar na interface)
                END --OR (sessions.personal_time * 1) <> sessions.personal_time
            -- group by para nao repetir os resultados caso caia na condicao acima
            GROUP BY sessions.id""".format(s=seconds))
        for r in busca_acessos:
            self.fw_exclude_released_host(r['mac'])
            cursor.execute("""UPDATE sessions
                           SET date_time_end = datetime('now', 'localtime') WHERE id = ?""", (r['id'],))
        conexao.commit()
        #conexao.close()
        return True


    def block_host(self, mac=None, reason=None):
        if not self.validate_mac(mac):
            app.logger.warning('block_host: "%s" nao eh um mac valido' % mac)
            return False
        if not self.fw_exclude_released_host(mac):
            app.logger.warning('block_host: erro ao excluir usuario liberado no firewall. MAC eh "%s"' % mac)
            #return False
        conexao = self.obter_bd()
        cursor = conexao.cursor()
        finalizar_acesso = cursor.execute("""UPDATE
        sessions SET date_time_end = datetime('now', 'localtime')
        WHERE ifnull(date_time_end, '') = ''
        AND host_id = (SELECT id FROM hosts WHERE mac = ?)""", (mac,))
        conexao.commit()
        busca_mac_bloqueado = cursor.execute("SELECT id FROM blocked_hosts WHERE mac = ?", (mac,))
        if not busca_mac_bloqueado.fetchone():
            inserir_mac_bloqueado = cursor.execute("INSERT INTO blocked_hosts (mac, reason)"
                                                   "VALUES (?, ?)", (mac, reason))
            conexao.commit()
            if inserir_mac_bloqueado:
                return True
            else:
                return False
        return True


    def terminate_session(self, mac=None, session_id=None):
        """Termina uma sessão identificada pelo mac do host, id da sessao ou ambos"""
        if mac and not self.validate_mac(mac):
            app.logger.warning('terminate_session: "%s" nao eh um mac valido' % mac)
            return False

        endereco_mac = mac
        codigo_sessao = session_id
        if endereco_mac and not codigo_sessao:
            busca_sessao = self.buscar_bd("""SELECT id
            FROM sessions
            WHERE ifnull(date_time_end, '') = ''
            AND host_id = (SELECT id FROM hosts WHERE mac = ?)""", (endereco_mac,), True)
            if not busca_sessao:
                app.logger.warning('terminate_session: sessao nao encontrada para o mac %s' % endereco_mac)
                return False
            codigo_sessao = busca_sessao['id']
        if codigo_sessao and not endereco_mac:
            busca_mac = self.buscar_bd("""SELECT hosts.mac
            FROM sessions, hosts
            WHERE sessions.host_id = hosts.id
            AND sessions.id = ?""", (codigo_sessao,), True)
            if not busca_mac:
                app.logger.warning('terminate_session: mac nao encontrado para o host da sessao %s' % codigo_sessao)
                return False
            endereco_mac = busca_mac['mac']
        if not endereco_mac and not codigo_sessao:
            app.logger.warning('terminate_session: erro nos valores do mac(%s) ou codigo da'
                               'sessao(%s)' % (endereco_mac, codigo_sessao))
            return False

        if not self.fw_exclude_released_host(endereco_mac):
            app.logger.warning('terminate_session: erro ao excluir usuario liberado no firewall.'
                               'MAC eh "%s"' % endereco_mac)
            #return False
        conexao = self.obter_bd()
        cursor = conexao.cursor()
        finalizar_sessao = cursor.execute("""UPDATE
        sessions SET date_time_end = datetime('now', 'localtime')
        WHERE id = ?""", (codigo_sessao,))
        conexao.commit()
        return True


    def daemon_clean_users(self):
        """Fica em loop executando o metodo clean_expired_session"""
        while True:
            self.clean_expired_session()
            time.sleep(60)


    def pyro_test(self):
        """Método para ser usado apenas para testar uma conexão via pyro"""
        return True


    def main(self):
        # inicia uma thread para limpar os usuarios conectados ha mais tempo do que o definido
        self.threaddaemon_clean_users = thread.start_new_thread(self.daemon_clean_users, tuple())


if __name__ == '__main__':
    print "Iniciando o GCP daemon"
    #FIXME fixar permissoes do arquivo de log
    # Exceto pelo Formatter, código idêntico ao contido no www/gcp_web.py
    app.config.from_pyfile(os.path.join(app.root_path, '../config/gcp.cfg'))
    # Obtem as configuracoes de um arquivo especificado pela variavel de ambiente GCP_SETTINGS, caso exista
    app.config.from_envvar('GCP_SETTINGS', silent=True)
    ponteiro = RotatingFileHandler(os.path.join(app.root_path, app.config['GCP_LOG']), maxBytes=10000, backupCount=1)
    ponteiro.setLevel(logging.INFO)
    formato = logging.Formatter("%(asctime)s - gcpd - %(levelname)s - %(message)s")
    ponteiro.setFormatter(formato)
    app.logger.addHandler(ponteiro)

    instancia_gcp = GCP()
    instancia_gcp.main()
    daemon = Pyro4.Daemon()
    uri = daemon.register(instancia_gcp)
    caminho_arquivo_uri = os.path.join(tempfile.gettempdir(), 'gcpd_uri.txt')
    with open(caminho_arquivo_uri, 'w') as f:
        f.write(str(uri))
        f.close()
    print "GCPD pronto. A uri eh ", uri
    daemon.requestLoop()

    thread.exit()
    print "FIM"
