#!/usr/bin/env python
#--*-- coding: utf-8 --*--

from flask import Flask
import Pyro4
import argparse
import os
import tempfile
import sys

app = Flask(__name__)

def main():
    caminho_arquivo_uri = os.path.join(tempfile.gettempdir(), 'gcpd_uri.txt')
    with open(caminho_arquivo_uri) as f:
        uri = f.read().strip()
        instancia_gcp = Pyro4.Proxy(uri)
        try:
            if not instancia_gcp.pyro_test():
                app.logger.error('Erro no retorno do metodo teste_pyro.')
                sys.exit(1)
        except Pyro4.errors.CommunicationError, e:
            app.logger.error('Erro ao comunicar com o daemon. Detalhes: {}'.format(e))
            sys.exit(1)

    parser = argparse.ArgumentParser(description='Executas algumas tarefas para o GNU Captive Portal')

    # por seguranca, nao permitir a alteracao dos comandos ou do chain
    parser.add_argument("-a", "--listrules", help="Lista as regras de firewall do GCP", action="store_true")
    parser.add_argument("-r", "--release", help="Libera o MAC especificado")
    parser.add_argument("-m", "--getmac", help="Obtem mac address do ip especificado")
    parser.add_argument("-c", "--createrules", help="Cria as regras de firewall do GCP", action="store_true")
    parser.add_argument("-d", "--delete", help="Remove os usuarios que estao conectados ha mais segundos do que o especificado")
    #parser.add_argument("-l", "--listconnections", help="Lista as conexoes ativas", action="store_true")
    parser.add_argument("-b", "--blockmac", help="Bloqueia o mac address especificado")

    opcoes = parser.parse_args()

    r = None
    if opcoes.listrules:
        r = instancia_gcp.fw_list_rules()
    if opcoes.release:
        r = instancia_gcp.fw_inserir_usuario_liberado("{MAC}".format(MAC=opcoes.release))
    if opcoes.getmac:
        r = instancia_gcp.get_mac("{IP}".format(IP=opcoes.getmac))
    if opcoes.createrules:
        r = instancia_gcp.fw_create_rules()
    if opcoes.delete:
        r = instancia_gcp.limpar_usuarios_conectados("{s}".format(s=opcoes.delete))
    # if opcoes.listconnections:
    #     retorno = instancia_gcp.listar_conexoes_ativas()
    #     r = ""
    #     for i in retorno:
    #         r += i + "\n"
    if opcoes.blockmac:
        r = instancia_gcp.bloquear_usuario("{MAC}".format(MAC=opcoes.blockmac))

    if r:
        print r



if __name__ == "__main__":
    app.config.from_pyfile( os.path.join(app.root_path, '../config/gcp.cfg') )
    # Obtem as configuracoes de um arquivo especificado pela variavel de ambiente GCP_SETTINGS, caso exista
    app.config.from_envvar('GCP_SETTINGS', silent=True)
    main()

