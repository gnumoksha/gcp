# http://flask.pocoo.org/docs/deploying/mod_wsgi/
import sys

PATH_ENV_GCP='/path/env_gcp'
PATH_GCP='/path/env_gcp/gcp'

activate_this = PATH_ENV_GCP + '/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

sys.path.insert(0, PATH_GCP)

from www.gcp_web import app as application