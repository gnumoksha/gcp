// substitui o alert padrao do javascript, por este que utiliza
// jqueryui dialog
function dialog (mensagem, titulo) {
	$(function() {
		if (titulo == undefined || titulo == '') titulo = 'GCP';

		// previne que tenha mais de uma janela
		$( "#janela_alerta_unica:ui-dialog" ).dialog("destroy");

		var dialogo = $('<div id="janela_alerta_unica" title="'+titulo+'">\n\
						<p>'+mensagem+'</p>\n\
					</div>');

		dialogo.dialog({
				resizable: false,
				autoOpen: true,
				modal: true,
				buttons: {
				Ok: function() {
						$( this ).dialog( "close" );
					}
				}
			});
	});
	// previne que o browser siga o link
	return false;
}

function dialog_ajax(url, largura, altura, titulo) {
	$(function (){
		if (titulo == undefined || titulo == "") {
			titulo = "Liber";
		}

		// exibe um icone de carregamento, via css
		var dialog = $('<div style="display:none" class="carregando"></div>').appendTo('body');
		// abre o dialog
		dialog.dialog({
			// previne multiplas divs no documento
			close: function(event, ui) {
				// remove a div com todos os dados e eventos
				dialog.remove();
			},
			modal: false,
			width: largura,
			height: altura,
			title: titulo
		});

		// carrega conteudo remoto
		dialog.load (
			url,
			{}, // omitir este parametro emiti um pedido GET em vez de um pedido POST, caso contrário, você pode fornecer parâmetros post dentro do objeto
			function (responseText, textStatus, XMLHttpRequest) {
				// remove a classe que exibe a imagem de carregando
				dialog.removeClass('carregando');
			}
		);
		// previne que o browser siga o link
		return false;
	});
}

function question(text, title) {
	$(function() {
	    if (title == undefined || title == '') title = 'GCP';

		var $dialogo = $('<div title="'+title+'" style="display:none"><p><span class="ui-icon ui-icon-alert"'+
		'style="float:left; margin:0 7px 20px 0;"></span>'+text+'</p></div>');
		$dialogo.dialog({
				resizable: true,
				autoOpen: true,
				modal: true,
				buttons: {
					'Sim': function() {
						$(this).dialog( "close" );
						return true;
					},
					'Não': function() {
						$(this).dialog( "close" );
						return false;
					}
				}
			});
	});
	// previne que o browser siga o link
	return false;
}