#--*-- coding: utf-8 --*--
from flask import Flask, url_for, request, render_template, redirect, flash, g, session, jsonify
import os
import sqlite3
import logging
from logging.handlers import RotatingFileHandler
import Pyro4
import tempfile
from datetime import datetime
import re
import json

gcp_version = '0.6'
app = Flask(__name__)

app.config.from_pyfile(os.path.join(app.root_path, '../config/gcp.cfg'))
# Obtem as configuracoes de um arquivo especificado pela variavel de ambiente GCP_SETTINGS, caso exista
app.config.from_envvar('GCP_SETTINGS', silent=True)
ponteiro = RotatingFileHandler(os.path.join(app.root_path, app.config['GCP_LOG']), maxBytes=10000, backupCount=1)
#FIXME nao recebo mensagens menores que warning mesmo colocando level em debug com app.debug = false
ponteiro.setLevel(logging.INFO)
formato = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
ponteiro.setFormatter(formato)
app.logger.addHandler(ponteiro)

app.secret_key = app.config['GCP_SECRET_KEY']


def get_pyro_connection():
    """
    Retorna uma instância da class GCP que está em bin/gcpd.py, via Pyro
    Util para que nao seja necessario reiniciar o aplicativo se o gcpd for reiniciado
    """
    caminho_arquivo_uri = os.path.join(tempfile.gettempdir(), 'gcpd_uri.txt')
    if not os.path.exists(caminho_arquivo_uri):
        return False
    with open(caminho_arquivo_uri) as f:
        uri = f.read().strip()
        f.close()
        instancia_gcp = Pyro4.Proxy(uri)
        try:
            if not instancia_gcp.pyro_test():
                app.logger.error('get_pyro_connection: erro no retorno do metodo pyro_test.')
                return False
        except Pyro4.errors.CommunicationError, e:
            app.logger.error('get_pyro_connection: erro ao comunicar com o daemon. Detalhes: {}'.format(e))
            return False
        return instancia_gcp


def db_dict_factory(cursor, row):
    """
    Para o resultado das consultas virem em dicionario ao inves de objeto sqlrow
    http://stackoverflow.com/questions/3300464/how-can-i-get-dict-from-sqlite-query
    https://docs.python.org/2/library/sqlite3.html#sqlite3.Connection.row_factory
    """
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def db_get():
    """
    Retorna uma conexão com o banco de dados sqlite e armazena essa conexao em g._db_connection
    http://flask.pocoo.org/docs/patterns/sqlite3/#sqlite3
    """
    #with app.app_context():
    conexao_bd = g.get('_db_connection', None)
    if conexao_bd is None:
        conexao_bd = sqlite3.connect(os.path.join(app.root_path, app.config['GCP_DATABASE']))
        #conexao_bd.row_factory = sqlite3.Row
        conexao_bd.row_factory = db_dict_factory
        g._db_connection = conexao_bd
    return conexao_bd


@app.teardown_appcontext
def db_close(exception):
    """
    Fecha a conexão com o banco de dados que está armazenada em g._banco
    http://flask.pocoo.org/docs/patterns/sqlite3/#sqlite3
    """
    conexao_bd = g.get('_db_connection', None)
    if conexao_bd is not None:
        conexao_bd.close()


def db_search(query, args=(), only_one_result=False):
    """
    Busca query no banco.
    args é passado para o metodo execute do sqlite3
    http://flask.pocoo.org/docs/patterns/sqlite3/#sqlite3
    """
    cursor = db_get().execute(query, args)
    r = cursor.fetchall()
    cursor.close()
    return (r[0] if r else None) if only_one_result else r


def db_create():
    """
    from gcp_web import db_create
    db_create()
    http://flask.pocoo.org/docs/patterns/sqlite3/#sqlite3
    http://flask.pocoo.org/docs/tutorial/dbinit/#tutorial-dbinit
    """
    # Crio um contexto de aplicacao, pois esta funcao é chamada fora do ciclo comum do flask
    with app.app_context():
        banco = db_get()
        arquivo_schema = os.path.join(app.root_path, 'schema.sql')
        # open_resource abre um arquivo no dirtorio da aplicacao
        with app.open_resource(arquivo_schema, mode='r') as f:
            banco.cursor().executescript(f.read())
        banco.commit()


def db_execute(query, args=(), autocommit=True, empty_as_none=True):
    """
    Executa query no banco e retorna True se tudo ocorrer corretamente.
    Em caso de erro retorna uma string contendo a mensagem de erro.
    """

    # Faz com que argumentos vazios sejam definidos como null no banco
    if args and empty_as_none:
        t = []
        for arg in args:
            t.append(arg) if arg != '' else t.append(None)
        args = t

    db = db_get()
    cursor = db.cursor()
    try:
        cursor.execute(query, args)
    except sqlite3.OperationalError as e:
        return u"Erro operacional. Detalhes: %s" % e
    except sqlite3.IntegrityError as e:
        if str(e).find("UNIQUE constraint failed") >= 0:
            # exemplo: UNIQUE constraint failed: hosts.mac
            campo = str(e).split('.')[1]
            return u"O valor do campo %s já existe para outro registro." % campo
        elif str(e).find("NOT NULL constraint failed") >= 0:
            # exemplo: NOT NULL constraint failed: hosts.allow_user_edit
            campo = str(e).split('.')[1]
            return u"O campo %s não pode ser nulo." % campo
        else:
            return u"Erro de integridade. Detalhes: %s." % str(e)
    except:
        #TODO
        # Erro ao executar operação no banco de dados. Detalhes:
        raise
    if autocommit:
        db.commit()
    return True


def validate_mac(mac=None):
    if not mac:
        return False
    m = '[0-9a-f]{2}'
    if re.search('{m}:{m}:{m}:{m}:{m}:{m}'.format(m=m), mac, re.I):
        return True
    return False


@app.template_filter('display_version')
def display_version(s=''):
    """Gambiarra que exibe a versao do sistema, definido na variavel gcp_version.
    Recebe como argumento uma string que será concatenada na variavel gcp_version"""
    return "%s %s" % (s, gcp_version)


@app.template_filter('seconds_to_text')
def seconds_to_text(s):
    """Recebe um numero de segundos e retorna uma string contendo o numero de
    anos, semanas, dias, horas, minutos e segundos.

    Baseado em um ano de 365 dias"""
    try:
        s = int(s)
    except ValueError:
        return False
    except TypeError:
        return False

    # como nesta funcao cada ano tem 365 dias e nao 365,25 dias, a cada 4 anos
    # aumento um dia
    anos = s / 31536000  # segundos em um ano
    if anos >= 4:
        for i in xrange(0, anos / 4):
            s += 86400  # segundos em uma dia

    anos, resto = (s / 31536000, s % 31536000)  # segundos em um ano
    #TODO semanas parece nao funcionar para valores maiores
    #semanas, resto = (resto / 604800, resto % 604800)  # segundos em uma semana
    semanas = 0
    dias, resto = (resto / 86400, resto % 86400)  # segundos em um dia
    horas, resto = (resto / 3600, resto % 3600)  # segundos em uma hora
    minutos, resto = (resto / 60, resto % 60)  # segundos em um minuto
    segundos = resto

    texto = []
    if anos > 1:
        texto.append("%d anos" % anos)
    elif anos == 1:
        texto.append("1 ano")
    if semanas > 1:
        texto.append("%d semanas" % anos)
    elif semanas == 1:
        texto.append("1 semana")
    if dias > 1:
        texto.append("%d dias" % dias)
    elif dias == 1:
        texto.append("1 dia")
    if horas > 1:
        texto.append("%d horas" % horas)
    elif horas == 1:
        texto.append('1 hora')
    if minutos > 1:
        texto.append("%d minutos" % minutos)
    elif minutos == 1:
        texto.append('1 minuto')
    if segundos > 1:
        texto.append("%d segundos" % segundos)
    elif segundos == 1:
        texto.append("1 segundo")
    return ", ".join(texto)


@app.template_filter('formata_data_hora')
def formata_data_hora(data_hora, formato='br', formato_entrada="%Y-%m-%d %H:%M:%S", formato_saida="%d/%m/%Y %H:%M:%S"):
    """
    Formata 'data', que esta no formato 'formato_entrada' para uma string especificada pelo formato
    'formato_saida'. Esta função está disponível nos templates.
    'formato' pode ser
        'br' -> converte do formato americano para o brasileiro
        'en' -> converte do formato brasileiro para o americano
    """
    if not data_hora:
        return None
    if formato == 'br':
        formato_entrada = "%Y-%m-%d %H:%M:%S"
        formato_saida = "%d/%m/%Y %H:%M:%S"
    elif formato == 'en':
        formato_entrada = "%d/%m/%Y %H:%M:%S"
        formato_saida = "%Y-%m-%d %H:%M:%S"
    try:
        objeto_data = datetime.strptime(data_hora, formato_entrada)
    except ValueError:
        return data_hora
    return objeto_data.strftime(formato_saida)


def login_required(f):
    """
    See http://flask.pocoo.org/docs/0.10/patterns/viewdecorators/
    """
    from functools import wraps
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not session.get('logged'):
            #return redirect(url_for('login', next=request.url))
            return redirect(url_for('login'))
        data_hora_sessao = datetime.strptime(session.get('date_time'), "%Y-%m-%d %H:%M:%S")
        data_hora_atual = datetime.now()
        tempo_sessao = data_hora_atual - data_hora_sessao
        if tempo_sessao.total_seconds() > 3600:  # 3600 = 1 hora
            return redirect(url_for('logout'))
        session['date_time'] = data_hora_atual.strftime("%Y-%m-%d %H:%M:%S")
        return f(*args, **kwargs)
    return decorated_function


# HACK que faz com que erros 404 sejam redirecionados para o portal.
# Ao se redirecionar uma requisicao do usuario, via iptables, para este servidor web, o cliente
# vai procurar neste servidor a pagina que estava acessando, e eh improvavel que ela estaja aqui.
# Exemplo:
# cliente acessa http://site.com.br/materia/noticia, ao ser redirecionado ele vai procurar
# a URL /materia/noticia neste servidor.
@app.errorhandler(404)
def page_not_found(erro):
    return redirect(url_for('portal'))


def get_mac_by_ip(ip):
    """
    Consulta através do gcpd o endereço MAC para o ip especificado.
    Retorna False e define uma mensagem no flash caso ocorre algum erro.

    Nota: no flask o ip do usuario está em request.remote_addr
    """
    conexao_pyro = get_pyro_connection()
    if not conexao_pyro:
        flash(u'Não foi possível conectar ao daemon', 'danger')
        return False

    mac = conexao_pyro.get_mac(ip)
    if not mac:
        flash(u'Não foi possível determinar o endereço físico do dispositivo com IP %s' % ip, 'danger')
        return False
    return mac


def get_new_session_data_by_host(mac):
    """
    Retorna todos os campos da tabela access_types
    e o campo time que é tempo que durará uma nova sessão
    """
    if not mac:
        return False
    busca_dados_novo_acesso = db_search("""SELECT access_types.*,
    CASE WHEN ifnull(hosts.personal_time, 0) != 0 THEN -- 2
        hosts.personal_time
    WHEN ifnull(hosts.access_type_id, 0) != 0 AND ifnull(access_types.personal_time, 0) != 0 THEN -- 3
        (SELECT personal_time FROM access_types WHERE id = hosts.access_type_id)
    WHEN (SELECT ifnull(personal_time, 0) FROM access_types WHERE is_default = 1) != 0 THEN -- 4
        (SELECT personal_time FROM access_types WHERE is_default = 1)
    ELSE
        null
    END AS time
    FROM hosts, access_types
    WHERE access_types.id =
        CASE WHEN ifnull(hosts.access_type_id, '') != '' THEN
            hosts.access_type_id
        ELSE
            (SELECT id FROM access_types WHERE is_default = 1)
        END
    AND hosts.mac = ?""", (mac,), True)
    if not busca_dados_novo_acesso:
        # pega os dados do acesso padrao
        busca_dados_novo_acesso = db_search("""SELECT access_types.*,
        CASE WHEN ifnull(access_types.personal_time, 0) != 0 THEN
            access_types.personal_time
        ELSE
            null
        END AS time
        FROM access_types
        WHERE is_default = 1""", only_one_result=True)
    return busca_dados_novo_acesso


@app.route("/")
@app.route("/portal")
def portal():
    mac = get_mac_by_ip(request.remote_addr)
    if not mac:
        return render_template('portal.html')

    busca_dados = get_new_session_data_by_host(mac)
    if not busca_dados:
        flash(u'Falha ao obter os dados de uma nova sessão', 'danger')
    return render_template('portal.html', d=busca_dados)


@app.route('/user_term')
def user_term():
    mac = get_mac_by_ip(request.remote_addr)
    if not mac:
        return redirect(url_for('portal'))

    dados_nova_sessao = get_new_session_data_by_host(mac)
    if not dados_nova_sessao:
        return redirect(url_for('portal'))

    busca_termo = db_search("SELECT * FROM user_terms WHERE id = ?", (dados_nova_sessao['user_term_id'],), True)

    return render_template('user_term.html', d=busca_termo)


@app.route("/login", methods=['GET', 'POST'])
def login():
    erro = None
    if session.get('logged'):
        return redirect(url_for('dashboard'))

    if request.form.get('login') and request.method == "POST":
        if not request.form.get('user'):
            flash(u'Usuário não informado', 'danger')
            erro = True
        elif not request.form.get('password'):
            flash(u'Digite a senha', 'danger')
            erro = True

        if not erro:
            from werkzeug.security import check_password_hash
            buscar_user = db_search("""SELECT * FROM users
                WHERE user = ?""", (request.form['user'],), True)
            if buscar_user and buscar_user.get('is_active') and \
                    check_password_hash(str(buscar_user['password']), request.form['password']):
                session['logged'] = True
                session['user_id'] = buscar_user['id']
                session['user'] = buscar_user['user']
                session['user_name'] = buscar_user['name']
                session['date_time'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                return redirect(url_for('dashboard'))
            else:
                flash(u'Usuário e/ou senha incorretos', 'danger')

    return render_template('dashboard/login.html')


@app.route('/logout')
def logout():
    if session.get('logged'):
        session.pop('logged', None)
        session.pop('user_id', None)
        session.pop('user', None)
        session.pop('user_name', None)
        session.pop('date_time', None)
        flash(u'Você foi deslogado', 'success')
    return redirect(url_for('login'))


@app.route('/dashboard/')
@login_required
def dashboard():
    d = dict()

    numero_acessos_ativos = db_search("""SELECT COUNT(id) AS quantidade_ativos
    FROM sessions
    WHERE ifnull(date_time_end, '') = ''""", only_one_result=True)
    d['numero_acessos_ativos'] = numero_acessos_ativos['quantidade_ativos']

    d['top_numero_acessos'] = db_search("""SELECT sessions.host_id, COUNT(sessions.id) AS quantidade_acessos,
    hosts.name, hosts.device_description, device_types.description AS device_type
    FROM sessions
    LEFT OUTER JOIN hosts ON hosts.id = sessions.host_id
    LEFT OUTER JOIN device_types ON device_types.id = hosts.device_type_id
    GROUP BY sessions.host_id
    ORDER BY COUNT(sessions.id) DESC
    LIMIT 10""")

    d['top_tempo_acessos'] = db_search("""
    -- a lib sqlite incluiu o nome da tabela junto com nome da coluna, por isso renomeio
    SELECT acesso.host_id AS host_id,
    acesso.tempo_segundos AS total_tempo_segundos,
    hosts.name AS name,
    hosts.device_description AS device_description,
    device_types.description AS device_type
    FROM (
            --o sum() nao somava corretamente quando a query era uma soh e data_hora_fim era null
            SELECT host_id,
            sum(
                CASE WHEN ifnull(sessions.date_time_end, '') = '' THEN
                    ( strftime('%s', 'now', 'localtime') - strftime('%s', sessions.date_time_begin) )
                    ELSE ( strftime('%s', sessions.date_time_end) - strftime('%s', sessions.date_time_begin) )
                END
                ) AS tempo_segundos
            FROM sessions
            GROUP BY host_id
        ) AS acesso
    INNER JOIN hosts ON acesso.host_id = hosts.id
    LEFT OUTER JOIN device_types ON device_types.id = hosts.device_type_id
    ORDER BY acesso.tempo_segundos DESC
    LIMIT 10""")

    d['numero_hosts_cadastrados'] = db_search("SELECT count(*) AS c FROM hosts", only_one_result=True).get('c')

    #d['numero_hosts_bloqueados'] = db_search("SELECT count(*) AS c FROM blocked_hosts", only_one_result=True).get('c')

    #d['numero_codigos_nao_utilizados'] = db_search("""SELECT count(*) AS c
    #FROM codes WHERE used = 0""", only_one_result=True).get('c')

    d['numero_sessoes_finalizadas'] = db_search("""SELECT count(*) AS c
    FROM sessions WHERE ifnull(date_time_end, '') != ''""", only_one_result=True).get('c')

    d['unused_access_codes'] = db_search("SELECT count(*) AS c FROM codes WHERE used=0", only_one_result=True).get('c')

    valores_json = []
    for idx, busca in enumerate(db_search("""SELECT COUNT(sessions.id) AS number,
        hosts.name
        FROM sessions
        LEFT OUTER JOIN hosts ON hosts.id = sessions.host_id
        LEFT OUTER JOIN device_types ON device_types.id = hosts.device_type_id
        GROUP BY sessions.host_id
        ORDER BY COUNT(sessions.id) DESC
        LIMIT 10""")):
        valores = {
            "data": busca['number'],
            "label": busca['name'],
        }
        valores_json.append(valores)
    d['top_by_number'] = json.dumps(valores_json)

    valores_json = []
    for idx, busca in enumerate(db_search("""
    -- a lib sqlite incluiu o nome da tabela junto com nome da coluna, por isso renomeio
    SELECT acesso.tempo_segundos AS total_seconds,
    hosts.name AS name
    FROM (
            --o sum() nao somava corretamente quando a query era uma soh e data_hora_fim era null
            SELECT host_id,
            sum(
                CASE WHEN ifnull(sessions.date_time_end, '') = '' THEN
                    ( strftime('%s', 'now', 'localtime') - strftime('%s', sessions.date_time_begin) )
                    ELSE ( strftime('%s', sessions.date_time_end) - strftime('%s', sessions.date_time_begin) )
                END
                ) AS tempo_segundos
            FROM sessions
            GROUP BY host_id
        ) AS acesso
    INNER JOIN hosts ON acesso.host_id = hosts.id
    LEFT OUTER JOIN device_types ON device_types.id = hosts.device_type_id
    ORDER BY acesso.tempo_segundos DESC
    LIMIT 10""")):
        valores = {
            "data": busca['total_seconds'],
            "label": busca['name'],
        }
        valores_json.append(valores)
    d['top_by_time'] = json.dumps(valores_json)

    return render_template('dashboard/index.html', d=d)


@app.route('/dashboard/sessions')
@login_required
def sessions():
    campos_ordem = ['sessions.id', 'sessions.date_time_begin', 'hosts.name', 'hosts.device_type_id',
                    'hosts.device_description', 'sessions.ip_address', 'access_types.id', 'hosts.mac',
                    'sessions.personal_time', 'sessions.browser_name', 'sessions.os_name', 'sessions.dist_name']
    campo_ordem = request.args.get('order', 'sessions.id', type=str)
    if campo_ordem not in campos_ordem:
        campo_ordem = 'sessions.id'

    #TODO ordenar pelos valores do case
    sessoes = db_search("""SELECT sessions.id, hosts.mac, hosts.name,
    hosts.device_description, sessions.personal_time, sessions.date_time_begin, sessions.ip_address, sessions.hostname,
    access_types.name AS tipo_acesso,
    sessions.user_agent, sessions.browser_name, sessions.browser_version, sessions.os_name, sessions.os_version,
    sessions.dist_name, sessions.dist_version, sessions.platform_name, sessions.platform_version,
    (SELECT description FROM device_types WHERE id = hosts.device_type_id) AS device_type,
    -- seguir a mesma ordem que esta no metodo clean_expired_session do gcpd
    -- caso o tempo seja 0, ele não é considerado pois 0 = não há tempo
    --
    -- note que aqui o tempo é somado, nao subtraido
    CASE WHEN hosts.always_released = 1 THEN
        null
    WHEN ifnull(sessions.personal_time, 0) != 0 THEN -- 1
        datetime(sessions.date_time_begin, "+" || sessions.personal_time || " seconds")
    WHEN ifnull(hosts.personal_time, 0) != 0 THEN -- 2
        datetime(sessions.date_time_begin, "+" || hosts.personal_time || " seconds")
    WHEN ifnull(access_types.personal_time, 0) != 0 THEN -- 3
        datetime(sessions.date_time_begin, "+" || access_types.personal_time || " seconds")
    WHEN (SELECT ifnull(personal_time, 0) FROM access_types WHERE is_default = 1) != 0 THEN -- 4
        datetime(sessions.date_time_begin, "+" || (SELECT personal_time FROM access_types WHERE is_default = 1) || " seconds")
    ELSE
        null
    END acesso_valido_ate,
    -- de onde vem o tempo do acesso
    CASE WHEN ifnull(sessions.personal_time, 0) != 0 THEN -- 1
        'session'
    WHEN ifnull(hosts.personal_time, 0) != 0 THEN -- 2
        'host'
    WHEN ifnull(access_types.personal_time, 0) != 0 THEN -- 3
        'session_access_type'
    WHEN (SELECT ifnull(personal_time, 0) FROM access_types WHERE is_default = 1) != 0 THEN -- 4
        'default'
    ELSE
        null
    END AS origem_tempo,
    -- qual o tempo do acesso
    CASE WHEN ifnull(sessions.personal_time, 0) != 0 THEN -- 1
        sessions.personal_time
    WHEN ifnull(hosts.personal_time, 0) != 0 THEN -- 2
        hosts.personal_time
    WHEN ifnull(access_types.personal_time, 0) != 0 THEN -- 3
        access_types.personal_time
    WHEN (SELECT ifnull(personal_time, 0) FROM access_types WHERE is_default = 1) != 0 THEN -- 4
        (SELECT ifnull(personal_time, 0) FROM access_types WHERE is_default = 1)
    ELSE
        null
    END tempo
    --
    FROM sessions, hosts, access_types
    WHERE sessions.host_id = hosts.id
    AND sessions.access_type_id = access_types.id
    AND ifnull(sessions.date_time_end, '') = ''
    ORDER BY {o}""".format(o=campo_ordem))

    return render_template('dashboard/sessions.html', acessos=sessoes)


@app.route('/dashboard/sessions/terminate', methods=['GET'])
def terminate_session():
    erro = False

    mac = request.args.get('mac', None, type=str)
    sessao = request.args.get('id', None, type=int)

    if not erro and (mac or sessao):
        conexao_pyro = get_pyro_connection()
        if not conexao_pyro:
            flash(u'Não foi possível conectar ao daemon', 'danger')
            erro = True
        if mac and not validate_mac(mac):
            flash(u'MAC inválido', 'danger')
            erro = True

        if not erro:
            r = conexao_pyro.terminate_session(mac=mac, session_id=sessao)
            if r:
                flash(u'Sessão finalizada', 'success')
            else:
                flash(u'Erro ao finalizar a sessão', 'danger')

    return redirect(url_for('sessions'))


@app.route('/dashboard/hosts')
@login_required
def hosts():
    campo_ordem = request.args.get('order', 'hosts.id', type=str)
    campos_ordem = ['hosts.id', 'hosts.date_time', 'hosts.name', 'hosts.email', 'hosts.device_type_id',
                    'hosts.device_description', 'hosts.mac', 'hosts.always_released', 'hosts.access_type_id']
    if campo_ordem not in campos_ordem:
        campo_ordem = 'hosts.id'
    #TODO ordenar pelos valores dos selects extras
    h = db_search("""SELECT hosts.*,
    (SELECT date_time_begin FROM sessions WHERE host_id = hosts.id ORDER BY id DESC LIMIT 1) as ultimo_acesso,
    blocked_hosts.date_time AS bloqueado,
    device_types.description AS device_type,
    access_types.name AS access_type,
    ouis.organization AS oui_organization
    FROM hosts
    LEFT OUTER JOIN blocked_hosts ON blocked_hosts.mac = hosts.mac -- ORDER BY id DESC LIMIT 1
    LEFT OUTER JOIN device_types ON device_types.id = hosts.device_type_id
    LEFT OUTER JOIN access_types ON access_types.id = hosts.access_type_id
    LEFT OUTER JOIN ouis ON replace(substr(hosts.mac,0,9), ':', '-') = ouis.mac_hex
    ORDER BY %(q)s""" % {'q': campo_ordem})
    return render_template('dashboard/hosts/hosts.html', hosts=h)


@app.route('/dashboard/hosts/edit/<int:id>', methods=['GET', 'POST'])
@app.route('/dashboard/hosts/new', methods=['GET', 'POST'])
@login_required
def edit_host(id=None):
    erro = False
    d = dict()
    d['operacao'] = None

    if id:  # atualizando
        d['host'] = db_search("SELECT * FROM hosts WHERE id = ?", (id,), True)
        if not d['host']:
            flash(u'Host não encontrado', 'danger')
            return redirect(url_for('hosts'))
        d['operacao'] = "atualizado"
    else:  # Novo
        d['host'] = None
        d['operacao'] = "criado"

    d['devices'] = db_search("SELECT id, description FROM device_types")
    d['accesses'] = db_search("SELECT id, name FROM access_types")

    if 'submit' in request.form:
        d['host'] = request.form
        if not request.form.get('name'):
            flash(u'Preencha o nome', 'danger')
            erro = True
        if not request.form['mac'] or not validate_mac(request.form['mac']):
            flash(u'MAC inválido', 'danger')
            erro = True
        elif request.form['device_type_id'] and not any(int(request.form['device_type_id']) == e['id'] for e in d['devices']):
            flash(u'Tipo de dispositivo inválido', 'danger')
            erro = True
        elif request.form['access_type_id'] and not any(int(request.form['access_type_id']) == e['id'] for e in d['accesses']):
            flash(u'Tipo de acesso inválido', 'danger')
            erro = True
        elif request.form['personal_time'] and not isinstance(request.form['personal_time'], int) and not request.form['personal_time'].isdigit():
            flash(u'Somente números são permitidos no campo referente ao tempo', 'danger')
            erro = True
        always_released = True if 'always_released' in request.form else False
        allow_user_edit = True if 'allow_user_edit' in request.form else False

        if not erro:
            h = d['host']
            valores = [h['name'], h['email'], h['device_type_id'], h['device_description'], h['mac'],
                       always_released, h['personal_time'], h['access_type_id'], allow_user_edit]
            if d['operacao'] == "criado":
                query = """INSERT INTO hosts
                (name, email, device_type_id, device_description, mac,
                always_released, personal_time, access_type_id, allow_user_edit)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"""
            elif d['operacao'] == "atualizado":
                valores.append(id)  # id para a clausula where
                query = """UPDATE hosts SET
                name = ?, email = ?, device_type_id = ?, device_description = ?, mac = ?,
                always_released = ?, personal_time = ?, access_type_id = ?, allow_user_edit = ?
                WHERE id = ?"""
            else:
                flash(u'Erro ao definir o tipo de operação', 'danger')
                return redirect(url_for('hosts'))
            r = db_execute(query, valores)
            if r is not True:
                flash(r, 'danger')
            else:
                flash(u'Host "%s" foi %s' % ((h['name'] or id), d['operacao']), 'success')
                return redirect(url_for('hosts'))

    return render_template('dashboard/hosts/edit.html', d=d)


@app.route('/dashboard/ajax/hosts/search', methods=['GET'])
def ajax_search_host():
    campo_pesquisar = request.args.get('field_search', 'name', type=str)
    termo = request.args.get('q', None, type=str)
    campo_retornar = request.args.get('field_return', 'mac', type=str)
    # Aparentemente nao é trivial pegar a lista de columas de uma tabela no sqlite
    # select * from sqlite_master e regex
    campos = ['id', 'date_time', 'name', 'email', 'device_type_id', 'device_description',
              'mac', 'always_released']
    retorno = {}

    if campo_pesquisar not in campos:
        app.logger.debug("Campo %s nao esta disponivel para pesquisa" % campo_pesquisar)
        campo_pesquisar = 'name'
    if campo_retornar not in campos:
        app.logger.debug("Campo %s nao esta disponivel para retorno" % campo_retornar)
        campo_retornar = 'mac'

    # usar ? para referir-se ao campo da tabela no where nao deu certo
    busca = db_search("SELECT * FROM hosts WHERE {} LIKE '%' || ? || '%'".format(campo_pesquisar), (termo,))
    i = 0
    for r in busca:
        retorno[i] = {
            'id': r['id'],
            'label': r['name'] or "Sem nome",
            'value': r[campo_retornar],
            campo_pesquisar: r[campo_pesquisar],
            campo_retornar: r[campo_retornar]
        }
        i += 1
    return jsonify(retorno)


@app.route('/dashboard/hosts/block', methods=['GET', 'POST'])
def block_host():
    erro = False
    mac = None
    motivo = None

    if 'mac' in request.form:
        mac = request.form['mac']
        motivo = request.form['reason'] if 'reason' in request.form else None
    elif 'mac' in request.values:
        mac = request.values['mac']
        motivo = request.values['reason'] if 'reason' in request.values else None

    if not erro and mac:
        conexao_pyro = get_pyro_connection()
        if not conexao_pyro:
            flash(u'Não foi possível conectar ao daemon', 'danger')
            erro = True
        if not validate_mac(mac):
            flash(u'MAC inválido', 'danger')
            erro = True

        if not erro:
            r = conexao_pyro.block_host(mac, motivo)
            if r:
                flash(u'MAC %s bloqueado' % mac, 'success')
                return redirect(url_for('blocked_hosts'))
            else:
                flash(u'Erro ao bloquear mac %s' % mac, 'danger')

    return render_template('dashboard/hosts/block.html')


@app.route('/dashboard/hosts/unblock', methods=['GET'])
def unblock_host():
    mac = request.args.get('mac', None, type=str)
    if mac:
        r = db_execute("DELETE FROM blocked_hosts WHERE mac = ?", (mac,))
        if r is True:
            flash(u'MAC "%s" desbloqueado' % mac, 'success')
        else:
            flash(u'Erro ao desbloquear MAC "%s". Detalhes: %s' % (mac, r), 'danger')

    return redirect(url_for('blocked_hosts'))


@app.route('/dashboard/hosts/blocked')
@login_required
def blocked_hosts():
    campos_ordem = ['blocked.id', 'blocked.date_time', 'hosts.name', 'blocked.mac',
                    'blocked.reason', 'device_types.description', 'hosts.device_description']
    campo_ordem = request.args.get('order', 'blocked.id', type=str)
    if campo_ordem not in campos_ordem:
        campo_ordem = 'blocked.id'

    #TODO ordenar pelos valores dos selects extras
    bloqueados = db_search("""SELECT blocked.date_time, blocked.mac, blocked.reason,
    hosts.name AS nome_host,
    hosts.device_description AS device_description,
    device_types.description AS device_type,
    (SELECT date_time_begin
        FROM sessions WHERE host_id = hosts.id
        ORDER BY id DESC LIMIT 1
    ) AS last_access
    FROM blocked_hosts blocked
    LEFT OUTER JOIN hosts ON hosts.mac = blocked.mac
    LEFT OUTER JOIN device_types ON device_types.id = hosts.device_type_id
    ORDER BY {o}""".format(o=campo_ordem))
    return render_template('dashboard/hosts/blocked.html', bloqueados=bloqueados)


@app.route('/dashboard/hosts/detail/', methods=['GET', 'POST'])
@login_required
def detail_host(id=None):
    d = dict()
    id = request.args.get('id', None, type=int)

    d['host'] = db_search("""
    SELECT hosts.*,
    device_types.description AS device_type,
    access_types.name AS access_type,
    ouis.organization AS oui_organization
    FROM hosts
    LEFT OUTER JOIN device_types ON device_types.id = hosts.device_type_id
    LEFT OUTER JOIN access_types ON access_types.id = hosts.access_type_id
    LEFT OUTER JOIN ouis ON replace(substr(hosts.mac,0,9), ':', '-') = ouis.mac_hex
    WHERE hosts.id = ?""", (id,), only_one_result=True)
    if not d['host']:
        flash(u'Host não encontrado', 'danger')
        error = True

    return render_template('dashboard/hosts/detail.html', d=d)


@app.route('/dashboard/codes/get', methods=['GET', 'POST'])
@login_required
def get_codes():
    erro = None
    dados = dict()
    codigos = []
    incluir_nao_utilizados = False
    termo_substituir = '%code%'

    if 'enviar' in request.form:
        dados = request.form
        if not request.form['quantidade']:
            flash(u'Peencha o campo quantidade', 'danger')
            erro = True
        elif not request.form['quantidade'].isdigit():
            flash(u'Somente números são permitidos no campo quantidade', 'danger')
            erro = True
        elif not request.form['quantidade_por_linha']:
            flash(u'Preencha o campo quantidade por linha', 'danger')
            erro = True
        elif not request.form['mensagem']:
            flash(u'Preencha o campo mensagem', 'danger')
            erro = True
        elif not re.search(termo_substituir, request.form['mensagem']):
            flash(u'O campo mensagem precisa conter o termo {}'.format(termo_substituir), 'danger')
            erro = True
        if 'nao_utilizados' in request.form:
            incluir_nao_utilizados = True

        if not erro:
            bd = db_get()
            cursor = bd.cursor()
            # atualiza opcoes desta rotina
            opcoes_rotina = {
                'get_codes': {
                    'amount': dados['quantidade'],
                    'amount_per_line': dados['quantidade_por_linha'],
                    'message': dados['mensagem'],
                    'include_non_used_codes': incluir_nao_utilizados,
                }
            }
            cursor.execute("""UPDATE routine_options SET get_codes = ? WHERE id = 1""", (json.dumps(opcoes_rotina),))
            bd.commit()
            numero_codigos_gerados = 0
            from random import randint
            while numero_codigos_gerados < int(request.form['quantidade']):
                n = "%d%d" % (randint(1000, 3999), randint(4000, 9999))
                busca_codigo = db_search("SELECT id FROM codes WHERE code = ?", (n,))
                if not busca_codigo:
                    numero_codigos_gerados += 1
                    codigos.append(n)
            for codigo in codigos:
                bd.execute("""INSERT INTO codes (code) VALUES(?)""", (codigo,))
            bd.commit()
            if incluir_nao_utilizados:
                buscar_nao_utilizados = db_search("SELECT code FROM codes WHERE used = 0")
                for codigo_nao_utilizado in buscar_nao_utilizados:
                    codigos.append(codigo_nao_utilizado['code'])
    else:
        consulta_opcoes = db_search("SELECT get_codes FROM routine_options WHERE id = 1", only_one_result=True)
        if consulta_opcoes and consulta_opcoes['get_codes']:
            opcoes_rotina = json.loads(consulta_opcoes['get_codes'])
            dados['quantidade'] = opcoes_rotina['get_codes']['amount'] or 102
            dados['quantidade_por_linha'] = opcoes_rotina['get_codes']['amount_per_line'] or 2
            dados['mensagem'] = opcoes_rotina['get_codes']['message'] or u'Código: {}'.format(termo_substituir)
            dados['nao_utilizados'] = opcoes_rotina['get_codes']['include_non_used_codes'] or False
        else:
            dados['quantidade'] = 102
            dados['quantidade_por_linha'] = 6
            dados['mensagem'] = u'Código: {}'.format(termo_substituir)

    return render_template('dashboard/codes/get.html', dados=dados, erro=erro, codigos=codigos,
                           termo_substituir=termo_substituir)


def generic_access(request_object, access_type):
    """Realiza rotinas comuns a um acesso.

    Recebe como parametro o objeto request e uma string com o nome do tipo de acesso

    Se a rotina retornar False, uma mensagem será definida pela funcao flash. Recomendo que
    redirecione para o portal.

    Retorna um dicionario:
    host -> is_registered Booleano
        caso registado, todos os campos da tabela host para o host
        MAC
        hostname
    use_term -> use_term Booleano
        caso use termos, todos os campos da tabela user_terms para aquele termo
    new_session -> access_types.*, time.
        Onde time é o tempo em segundos da sessao para o host
    pyro_connection -> objeto de conexão ao pyro
    """
    if not request_object:
        return False

    r = dict()

    dados_usuario = dict()
    dados_usuario['ip_address'] = request_object.remote_addr
    dados_usuario['user_agent'] = dict()
    dados_usuario['user_agent']['raw'] = request_object.headers.get('User-Agent')
     # dados extraidos do user agent
    ua = dict()
    # O Flask tem uma lib para isso, mas traz apenas nome da plataform, nome do browser,
    # versao do browser e linguagem
    #from werkzeug.useragents import UserAgent
    #UserAgent(request_object.headers.get('User-Agent')).atributo
    import httpagentparser
    # o parametro fill_none=True esta apenas trazendo uma chave version no final. Bug?
    d = httpagentparser.detect(dados_usuario['user_agent']['raw'])
    ua['browser_name'] = d.get('browser', {}).get('name', None)
    ua['browser_version'] = d.get('browser', {}).get('version', None)
    ua['os_name'] = d.get('os', {}).get('name', None)
    ua['os_version'] = d.get('os', {}).get('version', None)
    ua['dist_name'] = d.get('dist', {}).get('name', None)
    ua['dist_version'] = d.get('dist', {}).get('version', None)
    ua['platform_name'] = d.get('platform', {}).get('name', None)
    ua['platform_version'] = d.get('platform', {}).get('version', None)
    dados_usuario['user_agent']['processed'] = ua

    conexao_pyro = get_pyro_connection()
    if not conexao_pyro:
        flash(u'Não foi possível conectar ao daemon', 'danger')
        return False
    mac = conexao_pyro.get_mac(dados_usuario['ip_address'])
    if not mac:
        flash(u'Não foi possível determinar o endereço físico do dispositivo com IP %s' % dados_usuario['ip_address'],
              'danger')
        return False
    else:
        dados_usuario['mac'] = mac

    # Obtem o nome do host
    #TODO definir timeout. O timeout do proprio socket nao parece funcionar
    from socket import gethostbyaddr
    try:
        hostname = gethostbyaddr(dados_usuario['ip_address'])[0]
    except:
        hostname = None
    dados_usuario['hostname'] = hostname

    busca_mac_bloqueado = db_search("SELECT reason FROM blocked_hosts WHERE mac = ?", (dados_usuario['mac'],), True)
    if busca_mac_bloqueado:
        if busca_mac_bloqueado['reason']:
            flash(u'Desculpe, este dispositivo está bloqueado. Motivo: %s' % busca_mac_bloqueado['reason'], 'warning')
        else:
            flash(u'Desculpe, este dispositivo está bloqueado.', 'warning')
        return False

    busca_host = db_search("SELECT * FROM hosts WHERE mac = ?", (dados_usuario['mac'],), True)
    if busca_host:
        busca_acesso_ativo = db_search("SELECT date_time_begin FROM sessions WHERE ifnull(date_time_end, '') = '' "
                                       "AND host_id = ?", (busca_host['id'],), True)
        if busca_acesso_ativo:
            flash(u'Você já está conectado desde %s' % formata_data_hora(busca_acesso_ativo['date_time_begin']),
                  'warning')
            return False

    # busca dados de acesso para o host
    busca_dados_novo_acesso = get_new_session_data_by_host(dados_usuario['mac'])
    if not busca_dados_novo_acesso:
        flash(u'Erro ao consultar dados para uma nova sessão', 'danger')
        return False

    # busca termos de uso
    busca_termos_uso = db_search("SELECT * FROM user_terms WHERE id = ?",
                                 (busca_dados_novo_acesso['user_term_id'],), True)

    # Verifica se o tipo de acesso sendo feito está correto
    if not access_type or access_type != busca_dados_novo_acesso['name']:
        flash(u'O tipo de acesso atual está incorreto', 'danger')
        return False

    r['host'] = dados_usuario
    if busca_host:
        r['host'].update({'is_registered': True})
        r['host'].update(busca_host)
    else:
        r['host'].update({'is_registered': False})
    r['user_term'] = dict()
    if busca_termos_uso:
        r['user_term'].update({'use_term': True})
        r['user_term'].update(busca_termos_uso)
    else:
        r['user_term'].update({'use_term': False})
    r['new_session'] = busca_dados_novo_acesso
    r['pyro_connection'] = conexao_pyro
    return r


@app.route('/access/register_code', methods=['GET', 'POST'])
def access_by_register_code():
    erro = False
    dados_form = dict()
    dispositivos = db_search("SELECT * FROM device_types")
    d = generic_access(request, 'register_code')
    if not d:
        return redirect(url_for('portal'))

    # popula o formulario, caso o cliente exista
    if not request.form.get('enviar') and d['host']['is_registered']:
        dados_form = d['host']

    if request.form.get('enviar') and request.method == 'POST':
        dados_form = request.form
        if not request.form.get('name') or request.form.get('name').find(' ') == -1:
            flash(u'Preencha o campo com o nome completo', 'danger')
            erro = True
        elif not request.form.get('email'):
            flash(u'Preencha o campo e-mail', 'danger')
            erro = True
        elif not request.form.get('device_type_id'):
            flash(u'Preencha o campo do tipo de dispositivo', 'danger')
            erro = True
        elif not any(int(request.form.get('device_type_id')) == e['id'] for e in dispositivos):
            flash(u'Tipo de dispositivo inválido', 'danger')
            erro = True
        elif not request.form.get('device_description'):
            flash(u'Preencha o campo com a descrição do equipamento', 'danger')
            erro = True
        elif not request.form.get('code_id'):
            flash(u'Preencha o campo com o códdigo de acesso', 'danger')
            erro = True

        if not erro:
            busca_codigo_acesso = db_search("SELECT id FROM codes WHERE code = ? AND used = 0",
                                            (request.form['code_id'],), True)
            if not busca_codigo_acesso:
                flash(u"Código de acesso inválido", 'danger')
            else:
                bd = db_get()
                cursor = bd.cursor()
                r = request.form
                # valores comuns a inserção e atualização de um host
                valores_usuario = [r['name'], r['email'], r['device_type_id'],
                                   r['device_description']]
                r = None
                # cadastrar o usuario
                if not d['host']['is_registered']:
                    app.logger.debug('Inserindo host "%s"' % request.form['name'])
                    valores_usuario.append(d['host']['mac'])
                    cursor.execute("""INSERT INTO hosts
                        (name, email, device_type_id, device_description, mac)
                        VALUES (?, ?, ?, ?, ?)""", valores_usuario)
                    usuario_id = cursor.lastrowid
                # usuario ja cadastrado
                else:
                    if d['host']['allow_user_edit']:
                        app.logger.debug('Alterando host "%s"' % request.form['name'])
                        valores_usuario.append(0)  # valor para o allow_user_edit
                        valores_usuario.append(d['host']['id'])  # id do host no where
                        cursor.execute("""UPDATE hosts
                        SET name = ?, email = ?, device_type_id = ?, device_description = ?,
                        allow_user_edit = ?
                        WHERE id = ?""", valores_usuario)
                    usuario_id = d['host']['id']

                # inserir acesso
                app.logger.debug('Inserindo acesso para o host "%s"' % request.form['name'])
                # o tipo acesso da sessao é herdado do tipo de acesso do host
                tipo_acesso_sessao = d['host']['access_type_id'] if 'access_type_id' in d['host'] else None
                if not tipo_acesso_sessao:
                    tipo_acesso_sessao = db_search("""SELECT id FROM access_types WHERE is_default = 1""",
                                                   only_one_result=True)
                    tipo_acesso_sessao = tipo_acesso_sessao['id']

                ua = d['host']['user_agent']['processed']
                valores_inserir_acesso = (usuario_id, d['host']['ip_address'], d['host']['user_agent']['raw'],
                                          d['host']['hostname'],
                                          ua['browser_name'], ua['browser_version'], ua['os_name'], ua['os_version'],
                                          ua['dist_name'], ua['dist_version'], ua['platform_name'],
                                          ua['platform_version'], busca_codigo_acesso['id'], tipo_acesso_sessao)
                cursor.execute("""INSERT INTO sessions
                    (host_id, ip_address, user_agent, hostname, browser_name, browser_version, os_name,
                    os_version, dist_name, dist_version, platform_name, platform_version, code_id,
                    access_type_id)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""", valores_inserir_acesso)
                # invalidar código de acesso
                cursor.execute("""UPDATE codes set used = 1, used_by_host = ? WHERE id = ?""",
                               (usuario_id, busca_codigo_acesso['id']))
                # libera o usuário no firewall
                if not d['pyro_connection'].fw_insert_released_host(d['host']['mac']):
                    flash(u'Erro ao liberar o usuário no firewall', 'danger')
                    erro = True
                if not erro:
                    # grava as inserções
                    bd.commit()
                    if d['new_session']['url_redirect']:
                        return redirect(d['new_session']['url_redirect'])
                    flash(u'Você pode acessar à internet agora', 'success')
                    return redirect(url_for('portal'))
    return render_template('access/register_code.html', dispositivos=dispositivos, dados_form=dados_form,
                           new_session=d['new_session'], d=d)


@app.route('/access/register', methods=['GET', 'POST'])
def access_by_register():
    erro = False
    dados_form = dict()
    dispositivos = db_search("SELECT * FROM device_types")
    d = generic_access(request, 'register')
    if not d:
        return redirect(url_for('portal'))

    # popula o formulario, caso o cliente exista
    if not request.form.get('enviar') and d['host']['is_registered']:
        dados_form = d['host']

    if request.form.get('enviar') and request.method == 'POST':
        dados_form = request.form
        if not request.form.get('name') or request.form.get('name').find(' ') == -1:
            flash(u'Preencha o campo com o nome completo', 'danger')
            erro = True
        elif not request.form.get('email'):
            flash(u'Preencha o campo e-mail', 'danger')
            erro = True
        elif not request.form.get('device_type_id'):
            flash(u'Preencha o campo do tipo de dispositivo', 'danger')
            erro = True
        elif not any(int(request.form.get('device_type_id')) == e['id'] for e in dispositivos):
            flash(u'Tipo de dispositivo inválido', 'danger')
            erro = True
        elif not request.form.get('device_description'):
            flash(u'Preencha o campo com a descrição do equipamento', 'danger')
            erro = True

        if not erro:
            bd = db_get()
            cursor = bd.cursor()
            r = request.form
            # valores comuns a inserção e atualização de um host
            valores_usuario = [r['name'], r['email'], r['device_type_id'],
                               r['device_description']]
            r = None
            # cadastrar o usuario
            if not d['host']['is_registered']:
                app.logger.debug('Inserindo host "%s"' % request.form['name'])
                valores_usuario.append(d['host']['mac'])
                cursor.execute("""INSERT INTO hosts
                    (name, email, device_type_id, device_description, mac)
                    VALUES (?, ?, ?, ?, ?)""", valores_usuario)
                usuario_id = cursor.lastrowid
            # usuario ja cadastrado
            else:
                if d['host']['allow_user_edit']:
                    app.logger.debug('Alterando host "%s"' % request.form['name'])
                    valores_usuario.append(0)  # valor para o allow_user_edit
                    valores_usuario.append(d['host']['id'])  # id do host no where
                    cursor.execute("""UPDATE hosts
                    SET name = ?, email = ?, device_type_id = ?, device_description = ?,
                    allow_user_edit = ?
                    WHERE id = ?""", valores_usuario)
                usuario_id = d['host']['id']

            # inserir acesso
            app.logger.debug('Inserindo acesso para o host "%s"' % request.form['name'])
            # o tipo acesso da sessao é herdado do tipo de acesso do host
            tipo_acesso_sessao = d['host']['access_type_id'] if 'access_type_id' in d['host'] else None
            if not tipo_acesso_sessao:
                tipo_acesso_sessao = db_search("""SELECT id FROM access_types WHERE is_default = 1""",
                                               only_one_result=True)
                tipo_acesso_sessao = tipo_acesso_sessao['id']

            ua = d['host']['user_agent']['processed']
            valores_inserir_acesso = (usuario_id, d['host']['ip_address'], d['host']['user_agent']['raw'],
                                      d['host']['hostname'],
                                      ua['browser_name'], ua['browser_version'], ua['os_name'], ua['os_version'],
                                      ua['dist_name'], ua['dist_version'], ua['platform_name'],
                                      ua['platform_version'], tipo_acesso_sessao)
            cursor.execute("""INSERT INTO sessions
                (host_id, ip_address, user_agent, hostname, browser_name, browser_version, os_name,
                os_version, dist_name, dist_version, platform_name, platform_version, access_type_id)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""", valores_inserir_acesso)
            # libera o usuário no firewall
            if not d['pyro_connection'].fw_insert_released_host(d['host']['mac']):
                flash(u'Erro ao liberar o usuário no firewall', 'danger')
                erro = True
            if not erro:
                # grava as inserções
                bd.commit()
                if d['new_session']['url_redirect']:
                    return redirect(d['new_session']['url_redirect'])
                flash(u'Você pode acessar à internet agora', 'success')
                return redirect(url_for('portal'))
    return render_template('access/register.html', dispositivos=dispositivos, dados_form=dados_form,
                           new_session=d['new_session'], d=d)


@app.route('/access/code', methods=['GET', 'POST'])
def access_by_code():
    erro = False
    d = generic_access(request, 'code')
    if not d:
        return redirect(url_for('portal'))

    if 'submit' in request.form and request.method == 'POST':
        if not request.form['code_id']:
            flash(u'Preencha o campo com o códdigo de acesso', 'danger')
            erro = True

        if not erro:
            busca_codigo_acesso = db_search("SELECT id FROM codes WHERE code = ? AND used = 0",
                                            (request.form['code_id'],), True)
            if not busca_codigo_acesso:
                flash(u"Código de acesso inválido", 'danger')
            else:
                bd = db_get()
                cursor = bd.cursor()
                # cadastrar o usuario
                if not d['host']['is_registered']:
                    app.logger.debug('Inserindo host cujo mac eh "%s"' % d['host']['mac'])
                    cursor.execute("""INSERT INTO hosts
                        (mac)
                        VALUES (?)""", (d['host']['mac'],))
                    usuario_id = cursor.lastrowid
                # usuario ja cadastrado
                else:
                    usuario_id = d['host']['id']
                # inserir acesso
                app.logger.debug('Inserindo acesso para o host cujo mac eh "%s"' % d['host']['mac'])

                # o tipo acesso da sessao é herdado do tipo de acesso do host
                tipo_acesso_sessao = d['host']['access_type_id'] if 'access_type_id' in d['host'] else None
                if not tipo_acesso_sessao:
                    tipo_acesso_sessao = db_search("""SELECT id FROM access_types WHERE is_default = 1""",
                                                   only_one_result=True)
                    tipo_acesso_sessao = tipo_acesso_sessao['id']

                ua = d['host']['user_agent']['processed']
                valores_inserir_acesso = (usuario_id, d['host']['ip_address'], d['host']['user_agent']['raw'],
                                          d['host']['hostname'],
                                          ua['browser_name'], ua['browser_version'], ua['os_name'], ua['os_version'],
                                          ua['dist_name'], ua['dist_version'], ua['platform_name'],
                                          ua['platform_version'], busca_codigo_acesso['id'], tipo_acesso_sessao)
                cursor.execute("""INSERT INTO sessions
                    (host_id, ip_address, user_agent, hostname, browser_name, browser_version, os_name,
                    os_version, dist_name, dist_version, platform_name, platform_version, code_id,
                    access_type_id)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""", valores_inserir_acesso)
                # invalidar código de acesso
                cursor.execute("""UPDATE codes set used = 1, used_by_host = ? WHERE id = ?""",
                               (usuario_id, busca_codigo_acesso['id']))
                # libera o usuário no firewall
                if not d['pyro_connection'].fw_insert_released_host(d['host']['mac']):
                    flash(u'Erro ao liberar o usuário no firewall', 'danger')
                    erro = True
                if not erro:
                    # grava as inserções
                    bd.commit()
                    if d['new_session']['url_redirect']:
                        return redirect(d['new_session']['url_redirect'])
                    flash(u'Você pode acessar à internet agora', 'success')
                    return redirect(url_for('portal'))
    return render_template('access/code.html', new_session=d['new_session'])


@app.route('/access/user_term', methods=['GET', 'POST'])
def access_by_user_term():
    erro = False
    d = generic_access(request, 'user_term')
    if not d:
        return redirect(url_for('portal'))

    # usuario aceitou os termos
    if 'accept' in request.form:
        bd = db_get()
        cursor = bd.cursor()
        # cadastrar o usuario
        if not d['host']['is_registered']:
            app.logger.debug('Inserindo host cujo mac eh "%s"' % d['host']['mac'])
            cursor.execute("""INSERT INTO hosts
                (mac)
                VALUES (?)""", (d['host']['mac'],))
            usuario_id = cursor.lastrowid
        # usuario ja cadastrado
        else:
            usuario_id = d['host']['id']
        # inserir acesso
        app.logger.debug('Inserindo acesso para o host cujo mac eh "%s"' % d['host']['mac'])

        # o tipo acesso da sessao é herdado do tipo de acesso do host
        tipo_acesso_sessao = d['host']['access_type_id'] if 'access_type_id' in d['host'] else None
        if not tipo_acesso_sessao:
            tipo_acesso_sessao = db_search("""SELECT id FROM access_types WHERE is_default = 1""",
                                           only_one_result=True)
            tipo_acesso_sessao = tipo_acesso_sessao['id']

        ua = d['host']['user_agent']['processed']
        valores_inserir_acesso = (usuario_id, d['host']['ip_address'], d['host']['user_agent']['raw'],
                                  d['host']['hostname'],
                                  ua['browser_name'], ua['browser_version'], ua['os_name'], ua['os_version'],
                                  ua['dist_name'], ua['dist_version'], ua['platform_name'],
                                  ua['platform_version'], tipo_acesso_sessao)
        cursor.execute("""INSERT INTO sessions
            (host_id, ip_address, user_agent, hostname, browser_name, browser_version, os_name,
            os_version, dist_name, dist_version, platform_name, platform_version, access_type_id)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""", valores_inserir_acesso)
        # libera o usuário no firewall
        if not d['pyro_connection'].fw_insert_released_host(d['host']['mac']):
            flash(u'Erro ao liberar o usuário no firewall', 'danger')
            erro = True
        if not erro:
            # grava as inserções
            bd.commit()
            if d['new_session']['url_redirect']:
                return redirect(d['new_session']['url_redirect'])
            flash(u'Você pode acessar à internet agora', 'success')
            return redirect(url_for('portal'))

    return render_template('access/user_term.html', new_session=d['new_session'], user_term=d['user_term'])


@app.route('/dashboard/firewall/')
@app.route('/dashboard/firewall/<acao>', methods=['GET'])
@login_required
def firewall(acao=None):
    metodos_com_pyro = ['list_rules', 'create_rules', 'delete_rules']
    retorno = None
    erro = None

    if acao and acao in metodos_com_pyro:
        conexao_pyro = get_pyro_connection()
        if not conexao_pyro:
            flash(u'Não foi possível conectar ao daemon', 'danger')
            erro = True

    if erro:
        pass
    elif acao == 'list_rules':
        retorno = conexao_pyro.fw_list_rules()
        if retorno['result']:
            flash('Listando as regras', 'success')
        else:
            flash(u'Não há regras ativas', 'danger')
    elif acao == 'create_rules':
        retorno = conexao_pyro.fw_create_rules()
        if retorno['result']:
            flash('Regras criadas', 'success')
        else:
            flash('Erro ao criar as regras', 'danger')
    elif acao == 'delete_rules':
        retorno = conexao_pyro.fw_delete_rules()
        if retorno['result']:
            flash('Regras deletadas', 'success')
        else:
            flash('Erro ao deletar as regras', 'danger')

    return render_template('dashboard/firewall.html', retorno=retorno)


@app.route('/dashboard/utilities/')
@app.route('/dashboard/utilities/<acao>', methods=['GET', 'POST'])
@login_required
def utilities(acao=None):
    metodos_com_pyro = ['get_mac', 'wipe_connections']
    retorno = None
    erro = None

    if acao and acao in metodos_com_pyro:
        conexao_pyro = get_pyro_connection()
        if not conexao_pyro:
            flash(u'Não foi possível conectar ao daemon', 'danger')
            erro = True

    if erro:
        pass
    elif acao == 'get_mac':
        if not 'endereco_ip' in request.form or not request.form['endereco_ip']:
            flash(u'Preencha o campo com o endereço do ip', 'danger')
        else:
            retorno = conexao_pyro.get_mac(request.form['endereco_ip'])
            if retorno:
                flash(u'Obtido MAC do ip {}'.format(request.form['endereco_ip']), 'success')
            else:
                flash(u'Falha ao obter MAC do ip {}'.format(request.form['endereco_ip']), 'danger')
    elif acao == 'wipe_connections':
        if not 'numero_segundos' in request.form or not request.form['numero_segundos']:
            flash(u'Preencha o campo com o número de segundos', 'danger')
        elif not request.form['numero_segundos'].isdigit():
            flash(u'Somente números são aceitos', 'danger')
        else:
            retorno = conexao_pyro.clean_expired_session(request.form['numero_segundos'])
            if retorno:
                flash(u'Ação executada com sucesso', 'success')
            else:
                flash(u'Falha ao limpar as conexões', 'danger')

    return render_template('dashboard/utilities.html', retorno=retorno)


@app.route('/dashboard/access_types')
@login_required
def access_types():
    d = db_search("""SELECT access_types.*, user_terms.name AS user_term_name
    FROM access_types
    INNER JOIN user_terms ON access_types.user_term_id = user_terms.id""")
    return render_template('dashboard/access_types/access_types.html', d=d)


@app.route('/dashboard/access_types/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_access_type(id=None):
    erro = False
    d = dict()
    d['user_terms'] = db_search("SELECT id, name FROM user_terms")
    d['access_type'] = db_search("SELECT * FROM access_types WHERE id = ?", (id,), True)
    if not d['access_type']:
        flash(u'Tipo de acesso não encontrado', 'danger')
        return redirect(url_for('access_types'))

    if not request.form.get('submit'):
        form = d['access_type']

    if request.form.get('submit'):
        form = request.form
        is_default = True if request.form.get('is_default') else False
        if d['access_type']['is_default'] and not is_default:
            # no banco esta marcado que é o padrao mas o usuário desmarcou no formulario
            flash(u"""O tipo de acesso "%s" continua como padrão.
            Para definir um novo, edite o tipo de acesso desejado e marque-o como padrão""" % d['access_type']['name'],
                  'warning')
            is_default = True
        if not request.form.get('description'):
            flash(u'Preencha o campo descrição', 'danger')
            erro = True
        elif request.form.get('personal_time') and not request.form.get('personal_time').isdigit():
            flash(u'O tempo de acesso deve ser informado em segundos', 'danger')
            erro = True
        elif is_default and not request.form.get('personal_time'):
            flash(u'O tipo de acesso padrão precisa ter um tempo definido', 'danger')
            erro = True
        elif not request.form.get('user_term_id'):
            flash(u'O termo de uso que será utilizado é obrigatório', 'danger')
            erro = True

        if not erro:
            if is_default:
                # Todos sao definidos como falso e este que está sendo atualizado é definido mais a frente
                e = db_execute("UPDATE access_types SET is_default = 0", autocommit=False)
                if e is not True:
                    flash(e, 'danger')
                    return redirect(url_for('access_types'))
            r = request.form
            e = db_execute("""UPDATE access_types
            SET description = ?, personal_time = ?, is_default = ?, url_redirect = ?, user_term_id = ?
            WHERE id = ?""", (r.get('description'), r.get('personal_time'), is_default, r.get('url_redirect'),
                              r.get('user_term_id'), id))
            if e is not True:
                flash(e, 'danger')
            else:
                flash(u'Tipo de acesso "%s" foi atualizado' % d['access_type']['name'], 'success')
                return redirect(url_for('access_types'))

    return render_template('dashboard/access_types/edit.html', d=d, form=form)


@app.route('/dashboard/user_terms')
@login_required
def user_terms():
    d = db_search("SELECT * FROM user_terms")
    return render_template('dashboard/user_terms/user_terms.html', d=d)


@app.route('/dashboard/user_terms/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_user_term(id=None):
    erro = False
    d = dict()
    d['user_term'] = db_search("SELECT * FROM user_terms WHERE id = ?", (id,), True)
    if not d['user_term']:
        flash(u'Termo de uso não encontrado', 'danger')
        return redirect(url_for('user_terms'))

    if not request.form.get('submit'):
        form = d['user_term']

    if request.form.get('submit'):
        form = request.form
        if not request.form.get('name'):
            flash(u'Preencha o campo do nome', 'danger')
            erro = True
        elif not request.form.get('text'):
            flash(u'Preencha o texto do termo de uso', 'danger')
            erro = True

        if not erro:
            e = db_execute("""UPDATE user_terms
            SET name = ?, text = ?
            WHERE id = ?""", (request.form.get('name'), request.form.get('text'), id))
            if e is not True:
                flash(e, 'danger')
            else:
                flash(u'Termo "%s" foi atualizado' % d['user_term']['name'], 'success')
                return redirect(url_for('user_terms'))

    return render_template('dashboard/user_terms/edit.html', d=d, form=form)


@app.route('/dashboard/users')
@login_required
def users():
    d = db_search("SELECT * FROM users")
    return render_template('dashboard/users/users.html', d=d)


@app.route('/dashboard/users/edit/<int:id>', methods=['GET', 'POST'])
@app.route('/dashboard/users/new', methods=['GET', 'POST'])
@login_required
def edit_user(id=None):
    erro = False
    d = dict()
    d['operacao'] = None

    if id:  #atualizando
        d['user'] = db_search("SELECT * FROM users WHERE id = ?", (id,), True)
        if not d['user']:
            flash(u'Usuário não encontrado', 'danger')
            return redirect(url_for('users'))
        d['operacao'] = "atualizado"
    else:  #Novo
        d['user'] = None
        d['operacao'] = "criado"

    if request.form.get('submit'):
        d['user'] = request.form
        if not request.form.get('name'):
            flash(u'Preencha o campo do nome', 'danger')
            erro = True
        elif not request.form.get('user'):
            flash(u'Preencha o campo usuário', 'danger')
            erro = True
        elif re.search('[^a-z0-9_\-]', request.form.get('user'), re.IGNORECASE):
            flash(u'O nome de usuario pode conter letras, números, _ e -', 'danger')
            erro = True
        elif d['operacao'] == "criado" and not request.form.get('password'):
            flash(u'Preencha o campo da senha', 'danger')
            erro = True
        elif request.form.get('password') and not request.form.get('password_verify'):
            flash(u'Preencha a verificação da senha', 'danger')
            erro = True
        elif request.form.get('password') != request.form.get('password_verify'):
            flash(u'As senhas não são iguais', 'danger')
            erro = True
        elif db_search("SELECT id FROM users WHERE user = ? AND id != ?", (request.form.get('user'), (id or 0))):
            flash(u'Nome de usuário já está sendo utilizado', 'danger')
            erro = True

        is_active = True if request.form.get('is_active') else False

        if not erro:
            from werkzeug.security import generate_password_hash
            if request.form.get('password'):
                valores = [request.form.get('name'), request.form.get('user'),
                           generate_password_hash(request.form.get('password')), is_active]
            else:
                valores = [request.form.get('name'), request.form.get('user'), is_active]
            if d['operacao'] == "criado":
                query = """INSERT INTO users
                (name, user, password, is_active) VALUES (?, ?, ?, ?)"""
            elif d['operacao'] == "atualizado":
                valores.append(id)  # id para clausula where
                if request.form.get('password'):
                    query = """UPDATE users
                    SET name = ?, user = ?, password = ?, is_active = ?
                    WHERE id = ?"""
                else:
                    query = """UPDATE users
                    SET name = ?, user = ?, is_active = ?
                    WHERE id = ?"""
            else:
                flash(u'Erro ao definir tipo de operação', 'danger')
            e = db_execute(query, valores)
            if e is not True:
                flash(e, 'danger')
            else:
                flash(u'Usuário "%s" foi %s' % ((request.form.get('name') or id), d['operacao']), 'success')
                return redirect(url_for('users'))

    return render_template('dashboard/users/edit.html', d=d)


#EOF