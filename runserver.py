#--*-- coding: utf-8 --*--

from www.gcp_web import app
import os

if __name__ == "__main__":
    app.config.from_pyfile( os.path.join(app.root_path, '../config/gcp.cfg') )
    # Obtem as configuracoes de um arquivo especificado pela variavel de ambiente GCP_SETTINGS, caso exista
    app.config.from_envvar('GCP_SETTINGS', silent=True)

    # SERVER_NAME nao permite 0.0.0.0
    # http://flask.pocoo.org/docs/config/?highlight=server_name
    app.run(host=app.config['GCP_HOST'], port=int(app.config['GCP_PORT']), debug=app.config['GCP_DEBUG'])